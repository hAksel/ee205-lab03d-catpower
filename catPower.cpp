///////////////////////////////////////////////////////////////////////////////
///         University of Hawaii, College of Engineering
/// @brief  Lab 03d - CatPower - EE 205 - Spr 2022
///
/// Usage:  catPower fromValue fromUnit toUnit
///    fromValue: A number that we want to convert
///    fromUnit:  The energy unit fromValue is in
///    toUnit:  The energy unit to convert to
///
/// Result:
///   Print out the energy unit conversion
///
/// Example:
///   $ ./catPower 3.45e20 e j
///   3.45E+20 e is 55.2751 j
///
/// Compilation:
///   $ g++ -o catPower catPower.cpp
///
/// @file catPower.cpp
/// @version 1.0
///
/// @see https://en.wikipedia.org/wiki/Units_of_energy
/// @see https://en.wikipedia.org/wiki/List_of_unusual_units_of_measurement#Energy
///
/// @author Aksel Sloan <aksel@hawaii.edu> 
/// @date   2 February 2022 
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>

// A Joule represents the amount of electricity required to run a 1 W device for 1 s

const double EV_OVER_JOULE = 6.24150974e18;  //trust me I have a bs in physics
const double MEGATON_OVER_JOULE = 2.39e-16; 
const double GAS_OVER_JOULE = 8.244e-9; 
const double FOE_OVER_JOULE = 1e-24; 
const double CAT_OVER_JOULE = 0;

const char JOULE         = 'j';
const char ELECTRON_VOLT = 'e';
const char MEGATON_TNT = 'm';
const char GALLON_GAS = 'g';
const char FOE = 'f';
const char CAT_POWER = 'c';


double fromElectronVoltsToJoule( double electronVolts ) {
   return electronVolts / EV_OVER_JOULE;
}


double fromJouleToElectronVolts( double joule ) {
   return joule * EV_OVER_JOULE;
}

double fromMegatonToJoule( double megaton ) {
   return megaton / MEGATON_OVER_JOULE;
}

double fromJouleToMegaton( double joule ) {
   return joule * MEGATON_OVER_JOULE;
}

double fromGasToJoule (double gas ) {
   return gas / GAS_OVER_JOULE; 
}

double fromJouleToGas (double joule) {
   return joule * GAS_OVER_JOULE; 
}

double fromFoeToJoule (double foe ) {
   return foe / FOE_OVER_JOULE;
}

double fromJouleToFoe (double joule) {
   return joule * FOE_OVER_JOULE;
}


double fromCatPowerToJoule( double catPower ) {
   return 0.0;  // Cats do no work
}




int main( int argc, char* argv[] ) {
   printf( "Energy converter\n" );

   printf( "Usage:  catPower fromValue fromUnit toUnit\n" );
   printf( "   fromValue: A number that we want to convert\n" );
   printf( "   fromUnit:  The energy unit fromValue is in\n" );
   printf( "   toUnit:  The energy unit to convert to\n" );
   printf( "\n" );
   printf( "This program converts energy from one energy unit to another.\n" );
   printf( "The units it can convert are: \n" );
   printf( "   j = Joule\n" );
   printf( "   e = eV = electronVolt\n" );
   printf( "   m = MT = megaton of TNT\n" );
   printf( "   g = GGE = gasoline gallon equivalent\n" );
   printf( "   f = foe = the amount of energy produced by a supernova\n" );
   printf( "   c = catPower = like horsePower, but for cats\n" );
   printf( "\n" );
   printf( "To convert from one energy unit to another, enter a number \n" );
   printf( "it's unit and then a unit to convert it to.  For example, to\n" );
   printf( "convert 100 Joules to GGE, you'd enter:  $ catPower 100 j g\n" );
   printf( "\n" );

   double fromValue;
   char   fromUnit;
   char   toUnit;

   fromValue = atof( argv[1] );
   fromUnit = argv[2][0];         // Get the first character from the second argument
   toUnit = argv[3][0];           // Get the first character from the thrid argument

 //  printf( "fromValue = [%lG]\n", fromValue );   /// Remove before flight
 //  printf( "fromUnit = [%c]\n", fromUnit );      /// Remove before flight
//   printf( "toUnit = [%c]\n", toUnit );          /// Remove before flight

   int sudoBool = 1;
   double commonValue;
   switch( fromUnit ) {
      case JOULE         : commonValue = fromValue; // No conversion necessary
                           break;
      case ELECTRON_VOLT : commonValue = fromElectronVoltsToJoule( fromValue );
                           break;
      case MEGATON_TNT   : commonValue = fromMegatonToJoule (fromValue);
                           break;   //I guess spacing is important here?? (before :)
      case GALLON_GAS    : commonValue = fromGasToJoule ( fromValue ); 
                           break;
      case FOE           : commonValue = fromFoeToJoule ( fromValue ); 
                           break; 
      case CAT_POWER     : commonValue = fromCatPowerToJoule ( fromValue );
                           break; 
      default            :// printf("from unit default case triggered\n");
                           sudoBool = 0;
                          // printf("sudo bool = %i\n", sudoBool );
                           break;
   }
    
   //printf( "commonValue = [%lG] joule\n", commonValue ); /// Remove before flight

//use default to state if values are bad 
   double toValue;
   switch ( toUnit ) {
      case JOULE         : toValue = commonValue;
                        break;
      case ELECTRON_VOLT : toValue = fromJouleToElectronVolts ( commonValue );
                        break;
      case MEGATON_TNT   : toValue = fromJouleToMegaton ( commonValue );
                        break; 
      case GALLON_GAS    : toValue = fromJouleToGas ( commonValue );
                        break;
      case FOE           : toValue = fromJouleToFoe ( commonValue );
                        break;
      case CAT_POWER     : toValue = 8; 
                        break;
      default            :// printf("to unit default case triggered\n");
                           sudoBool = 0;
                           //printf("sudo bool = %i\n", sudoBool );
                        break;
   }
   if (sudoBool == 0){
      printf("Invalid unit input\n");
      return 0; 
   }
   printf( "%lG %c is %lG %c\n", fromValue, fromUnit, toValue, toUnit );
   if (toUnit == CAT_POWER) {
      printf("8 meaning infity here because #/0 = infinity \n");
   }
   if (fromUnit == CAT_POWER) {
      printf("because cats do no work \n");
   }
}
